﻿# Host: localhost  (Version 5.0.51b-community-nt-log)
# Date: 2017-04-12 15:08:01
# Generator: MySQL-Front 5.4  (Build 1.33)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "customer"
#

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customerid` int(10) NOT NULL auto_increment,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `sex` varchar(7) NOT NULL,
  `age` int(2) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY  (`customerid`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

#
# Data for table "customer"
#

/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (39,'123','123','Male',12,'0914183946','12'),(41,'supakit','namdet','Male',22,'0981611827','supakitzaaa@gmail.com');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
