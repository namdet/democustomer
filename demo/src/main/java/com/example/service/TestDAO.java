/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.bean.TestBean;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service
@Transactional
public class TestDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<TestBean> findAllCustommer() {
        return jdbcTemplate.query("select * from customer", MAPPER);
    }
    public TestBean findIdCustommer(int customerid) {
        String sql = "select * from customer where customerid = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{customerid},MAPPER);
    }

    public int update(TestBean testBean) {
        String sql = "update customer set fname=?,lname=?,sex=?,age=?,phone=?,email=? where customerid = ?";
        return jdbcTemplate.update(sql, new Object[]{testBean.getFname(), testBean.getLname(), testBean.getSex(),
            testBean.getAge(), testBean.getPhone(), testBean.getEmail(), testBean.getCustomerid()});
    }
    
     public int insert(TestBean testBean) {
        String sql = "insert into customer  (fname,lname,sex,age,phone,email) value (?,?,?,?,?,?)";
        return jdbcTemplate.update(sql, new Object[]{testBean.getFname(), testBean.getLname(), testBean.getSex(),
            testBean.getAge(), testBean.getPhone(), testBean.getEmail()});
    }
     
     public int delete(int customerid) {
        String sql = "delete from customer  where customerid = ?";//ยังไม่ได้
        return jdbcTemplate.update(sql , new Object[]{customerid});
    }
    

    public static final RowMapper<TestBean> MAPPER = new RowMapper<TestBean>() {
        @Override
        public TestBean mapRow(ResultSet rs, int rowNum) throws SQLException {
            TestBean o = new TestBean();
            o.setCustomerid(rs.getInt("customerid"));
            o.setFname(rs.getString("fname"));
            o.setLname(rs.getString("lname"));
            o.setSex(rs.getString("sex"));
            o.setAge(rs.getInt("age"));
            o.setPhone(rs.getString("phone"));
            o.setEmail(rs.getString("email"));

            return o;
        }

    };

}
