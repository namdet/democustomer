/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controller;

import com.example.bean.TestBean;
import com.example.service.TestDAO;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/demo/")

public class TestController {

    @Autowired
    TestDAO testDAO;

    @RequestMapping(value = "/findAll/", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody List<TestBean> findAll() {
        return testDAO.findAllCustommer();
    }
    
    @RequestMapping(value = "/getid/{customerid}/", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody TestBean getid(@PathVariable int customerid) {
        return testDAO.findIdCustommer(customerid);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody  int update(@PathVariable String id,@RequestBody TestBean testBean) {
        testBean.setCustomerid(new Integer(id));
        return testDAO.update(testBean);
    }
    
     @RequestMapping(value = "/insert/", method = RequestMethod.POST, headers = "Accept=application/json")
    public @ResponseBody  int insert(@RequestBody TestBean testBean) {
        return testDAO.insert(testBean);
    }
    
    @RequestMapping(value = "/delete/{customerid}/", method = RequestMethod.GET, headers = "Accept=application/json")
    public @ResponseBody  int delete(@PathVariable int customerid) {
        return testDAO.delete(customerid);
    }

}
